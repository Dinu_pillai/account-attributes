/*********************************************************************************************
Inside Sales Customer call list code
Author: Dinu Pillai
Last updated: 02/22/2021
Frequency: Weekly
Change description: Included customer_shipto_market_segment_desc, customer_shipto_state_customer_shipto_city
Updated the account filter to include 2021 market segments (14,15,16,17,19,20,33,34,50)
**********************************************************************************************/

--Create a base table with accounts managed by Inside sales team
drop table if exists reporting_cast.is_base;
create table reporting_cast.is_base as
select f.rpt_month,
       CAST(concat (strleft (CAST(f.rpt_month AS string),4),'-',strright (CAST(f.rpt_month AS string),2),'-','01') AS TIMESTAMP) month_id,
       f.load_source,
       --f.revenue_source,
       f.kci_sales_credit_flag,
       f.billable_flag,
       f.customer_shipto_key,
       f.customer_billto_key,
       --f.order_key,
       substr(t.sales_credit_type_code,1,3) territory_type,
       t.TERRITORY_CODE,
       t.salesrep_name,
       t.district district,
       t.region,
       t.geography,
       s.customer_shipto_zip_code,
       s.customer_shipto_class,
       s.customer_shipto_indirect_flag,
       S.customer_shipto_site_use_id,
       s.INS_FLAG MMC_FLAG,
       t.primary_flag,
       p.product_key,
       p.product_brand,
       p.product_sub_brand,
       p.smr_Product_Group,
       P.SALES_PRODUCT_GROUP,
       p.product_sku,
       p.product_item_description,
       P.PRODUCT_FAMILY,
       p.rentalsale_flag,
       s.customer_shipto_account_number Shipto_Customer_Number,
       s.customer_shipto_name Shipto_Customer_Name,
       s.customer_shipto_market_segment,
        --2021  Requirement
       upper(s.customer_shipto_market_segment_desc) market_segment,
       s.customer_shipto_city city,
       s.customer_shipto_state state,
       s.customer_sva_enabled_flag customer_sva_enabled_flag_cusshipto,
       b.customer_sva_enabled_flag customer_sva_enabled_flag_cusbillto,
       b.customer_billto_name,
       --r.customer_sva_enabled_flag customer_sva_enabled_flag_requester,
       s.customer_shipto_market_category,
       o.ro_number,
       s.inside_sales_flag,
	   (CASE WHEN load_source = 'ORDERS' THEN 'ORDERS' 
             WHEN f.load_source = 'REVENUE' AND p.product_brand LIKE 'DERMATAC%' --AND t.TERRITORY_CODE LIKE 'TSV%' 
			 AND UPPER(p.product_sub_type)='DRAPE' THEN 'DERMATAC'
             ELSE p.smr_product_group END) AS smr_product_group_updated,  
       g.customer_account_number,
       case when g.customer_account_number is null then 'No' else 'Yes' end as gold_flag,
       SUM(DECODE (f.billable_flag,'Y',f.commissionable_orders,'S',f.commissionable_orders,0)) ORDERS,
       SUM(f.avg_utility_old) avg_utility,
       SUM(f.RENTAL_REVENUE_AMOUNT) rental_rev,
       SUM(f.SALE_REVENUE_AMOUNT) sales_rev,
       SUM(f.RENTAL_REVENUE_AMOUNT + f.SALE_REVENUE_AMOUNT) revenue,
       SUM(f.TRANSITIONS) transitions,
       SUM(CASE WHEN f.load_source = 'REVENUE' then (f.RENTAL_REVENUE_AMOUNT +  f.SALE_REVENUE_AMOUNT) 
           ELSE DECODE(f.billable_flag,'Y',f.commissionable_orders,'S',f.commissionable_orders,0) END) revenue_plus_order 
from aim.aim_ordrev_mthly_snapshot_fact f
inner join aim.aim_customer_zipterr_xref_transposed_vw t on f.customer_shipto_key = t.customer_key and substr(t.sales_credit_type_code,1,3) in ('ISR','TSR')
inner join aim.aim_customer_shipto_dim s on f.customer_shipto_key = s.customer_shipto_key
inner join aim.aim_customer_billto_dim b ON f.customer_billto_key = b.customer_billto_key 
--LEFT JOIN aim.aim_requestor_dim r ON f.requestor_key = r.requestor_key
LEFT JOIN aim.aim_orders_dim o ON f.order_key = o.order_key
left join (SELECT * FROM aim.aim_product_dim WHERE etl_delete_flag = 'N') p on f.product_key = p.product_key
---Bring Gold Program customers
left join aim.gold_customer_roster g on g.customer_account_number = s.customer_shipto_account_number

where cast(s.customer_shipto_market_segment as integer) in(14,15,16,17,19,20,33,34,50)
and upper(s.customer_shipto_status) not in  ('PENDING')
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43;
Compute stats reporting_cast.is_base;
--2714392  row(s)


--Roll up revenue to site_use_id level
drop table if exists reporting_cast.is_rev_rollup;
Create table reporting_cast.is_rev_rollup as 
select rpt_month
,month_id
,customer_shipto_key 
,customer_billto_key
,customer_shipto_zip_code
,customer_shipto_class
,shipto_customer_number
,shipto_customer_name
--2021 Requirement
,market_segment
,city
,state
,customer_billto_name
,customer_shipto_site_use_id
,territory_code
,salesrep_name
,smr_product_group
,smr_product_group_updated
,gold_flag
,sum(revenue) revenue
from reporting_cast.is_base t1
where load_source ='REVENUE'
and (smr_product_group = 'CORE VAC' or  (rentalsale_flag = 'R' and product_brand in 
(

'ACTIV.A.C.™',
'V.A.C. SIMPLICITY™',
'INFOV.A.C.™',
'V.A.C. FREEDOM™',
'V.A.C.ULTA™',
'UNKNOWN',
'V.A.C. MISC'
)
)
)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18;
compute stats reporting_cast.is_rev_rollup;
--454778 row(s)


--Create base and prior period based on latest month
drop table if exists reporting_cast.is_seg_base_prior;
Create table reporting_cast.is_seg_base_prior as 
select a.*,
case when CAST(FROM_timestamp(add_months(now(),-6),'yyyyMM') AS INT) <= rpt_month and (CAST(FROM_timestamp(add_months(now(),-1),'yyyyMM') AS INT)>= rpt_month)  then revenue else 0 end as base_period,
case when (CAST(FROM_timestamp(add_months(now(),-12),'yyyyMM') AS INT) <= rpt_month) and (CAST(FROM_timestamp(add_months(now(),-6),'yyyyMM') AS INT)> rpt_month) then revenue else 0 end as prior_period
from reporting_cast.is_rev_rollup a;
compute stats reporting_cast.is_seg_base_prior;
--454778 row(s)

--Create segments
drop table if exists reporting_cast.is_customer_segments ;
create table reporting_cast.is_customer_segments as 
select t1.*, 
case when t1.base_revenue !=0  and t1.prior_revenue !=0 then 'Repeat'
    when  t1.base_revenue !=0 and t1.prior_revenue =0 then 'New'
    when  t1.base_revenue =0 and t1.prior_revenue !=0 then 'Lost'
    when  t1.base_revenue =0 and t1.prior_revenue =0 then 'Prospect' else 'NA' end as Segments
    from
    (
        select 
        --customer_shipto_key 
        --,customer_billto_key
         shipto_customer_number
        ,shipto_customer_name
        ,customer_shipto_zip_code
        ,customer_shipto_class
         --2021 Requirement
        ,market_segment
        ,city
        ,state
        --,customer_billto_name
        ,customer_shipto_site_use_id
        ,territory_code
        ,salesrep_name
        ,gold_flag
        ,sum(base_period) as base_revenue
        ,sum(prior_period) as prior_revenue
        from reporting_cast.is_seg_base_prior 
        group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1;
compute stats reporting_cast.is_customer_segments;
-- 20368 row(s) 

--Rules for priority
drop table if exists reporting_cast.is_rfm;
create table reporting_cast.is_rfm as
select shipto_customer_number
        ,shipto_customer_name
        ,customer_shipto_site_use_id
        ,customer_shipto_class
        --,segments
        ,cast(concat(strleft(cast(now() as string),7),'-','01') as timestamp)  as today
        ,int_months_between(cast(concat(strleft(cast(now() as string),7),'-','01') as timestamp) , max(case when recency >0 then month_id  end)) as Recency
        ,sum(Frequency) as Frequency
        ,sum(revenue) as Monetary
from
(
    select  b.rpt_month
        ,b.month_id
        ,b.shipto_customer_number
        ,b.shipto_customer_name
        ,b.customer_shipto_site_use_id
        ,b.customer_shipto_class
        ,s.segments
        ,sum(case when load_source ='ORDERS' and orders > 0 then orders else 0 end) as recency
        ,sum(case when load_source ='ORDERS' then orders else 0 end) as Frequency
        ,sum(case when load_source = 'REVENUE' then revenue else 0 end ) as revenue
    from reporting_cast.is_base b
    left join reporting_cast.is_customer_segments  s on b.customer_shipto_site_use_id = s.customer_shipto_site_use_id
    where rpt_month >= CAST(FROM_timestamp(add_months(now(),-12),'yyyyMM') AS INT)
    and rpt_month <= CAST(FROM_timestamp(add_months(now(),-1),'yyyyMM') AS INT)
    and (smr_product_group = 'CORE VAC' or  (rentalsale_flag = 'R' and product_brand in 
        (
        'ACTIV.A.C.™',
        'V.A.C. SIMPLICITY™',
        'INFOV.A.C.™',
        'V.A.C. FREEDOM™',
        'V.A.C.ULTA™',
        'UNKNOWN',
        'V.A.C. MISC'
        )
        )
        )
    and b.territory_type = 'ISR' --To avoid double counting the accounts
    and substr(s.territory_code,1,3)='ISR' --To avoid double counting the accounts
    group by 1,2,3,4,5,6,7
) a 
where segments <> 'Prospect'
group by 1,2,3,4,5;
compute stats reporting_cast.is_rfm;
--4568 row(s)


--Calculate percentage contribution
drop table if exists reporting_cast.is_rfm_pct;
create table reporting_cast.is_rfm_pct as
Select t2.*
,running_total_Monetary/total_monetary as percentage_monetary
,running_total_frequency/total_frequency as percentage_frequency
from
(
select t1.*
,sum(monetary) over (order by monetary desc rows between unbounded preceding and current row) as running_total_Monetary
,sum(monetary) over() as total_monetary
,sum(frequency) over (order by frequency desc rows between unbounded preceding and current row) as running_total_Frequency
,sum(frequency) over() as total_frequency
from reporting_cast.is_rfm t1
) t2;
compute stats reporting_cast.is_rfm_pct;
--4568 row(s)


--Assign scores
drop table if exists reporting_cast.is_rfm_score ;
create table reporting_cast.is_rfm_score as 
select t1.*
,case when percentage_monetary <= 0.25 then 4
        when percentage_monetary <= 0.5 then 3
        when percentage_monetary <= 0.75 then 2
        else 1 end as score_monetary

,case when percentage_frequency <= 0.25 then 4
        when percentage_frequency <= 0.5 then 3
        when percentage_frequency <= 0.75 then 2
        else 1 end as score_frequency
        
,case when recency <=3 then 4
        when recency <=6 then 3
        when recency <=9 then 2
        else 1 end as score_recency
from reporting_cast.is_rfm_pct t1;
compute stats reporting_cast.is_rfm_score;
--4568 row(s)


--Join all together
drop table if exists reporting_cast.inside_sales_call_list_all;
create table reporting_cast.inside_sales_call_list_all as
Select t1.territory_code
,t1.salesrep_name 
,t1.shipto_customer_number
,t1.shipto_customer_name
,t1.customer_shipto_zip_code
,t1.customer_shipto_class
--2021 Requirement
,t1.market_segment
,t1.city
,t1.state
,t1.customer_shipto_site_use_id
,case when t1.segments = 'Repeat' then 'Existing' else t1.segments end as segments
,t1.call_activity
,t1.Priority
,t1.gold_flag

from
(
Select distinct s.*
,c.score_recency
,c.score_frequency
,c.score_monetary
,c.score_recency + c.score_frequency + c.score_monetary as added_score
,p.Shipto_Customer_Number as Acc_placed_order

,case   when (c.score_recency + c.score_frequency + c.score_monetary) in (11,12,10) then 'High'
        when (c.score_recency + c.score_frequency + c.score_monetary) in (9,8,7) then 'Mid'
        when (c.score_recency + c.score_frequency + c.score_monetary) in (6,5,4,3) then 'Low'
     
else 'NA' end as Priority
,case  when segments in ('Repeat', 'New') and p.Shipto_Customer_Number is not null then 'Round call'
       When segments in ('Repeat', 'New') and p.Shipto_Customer_Number is  null then 'Marketing call' 
       When segments in ('Prospect') then 'Prospect call' 
       When segments in ('Lost') then 'Winback call' 
else'NA' end as call_activity
from reporting_cast.is_customer_segments s
left join reporting_cast.is_rfm_score c on s.customer_shipto_site_use_id = c.customer_shipto_site_use_id
left join 
(
select distinct customer_shipto_account_number as Shipto_Customer_Number,o.rop_placed_timestamp
from aim.aim_ordrev_mthly_snapshot_fact f
inner join aim.aim_customer_zipterr_xref_transposed_vw t on f.customer_shipto_key = t.customer_key and substr(t.sales_credit_type_code,1,3)  in ('ISR','TSR')
inner join aim.aim_customer_shipto_dim s on f.customer_shipto_key = s.customer_shipto_key
inner join aim.aim_customer_billto_dim b ON f.customer_billto_key = b.customer_billto_key 
--LEFT JOIN aim.aim_requestor_dim r ON f.requestor_key = r.requestor_key
LEFT JOIN aim.aim_orders_dim o ON f.order_key = o.order_key
left join (SELECT * FROM aim.aim_product_dim WHERE etl_delete_flag = 'N') p on f.product_key = p.product_key
where o.rop_placed_timestamp IS NOT NULL
--AND o.rop_void_verify_timestamp IS NULL
AND o.rop_stop_bill_timestamp IS NULL
--and cs.customer_shipto_market_segment in (14,15,16,19,33,34,50,99)
and cast(s.customer_shipto_market_segment as integer) in(14,15,16,17,19,20,33,34,50)
and upper(s.customer_shipto_status) not in  ('PENDING')
AND substr(t.sales_credit_type_code,1,3) in ('ISR','TSR')
and Upper(o.rop_status) ='PLACED'
) p on s.Shipto_Customer_Number = cast(p.Shipto_Customer_Number as string)
)t1;
compute stats reporting_cast.inside_sales_call_list_all;
--20368


---Defining priority for Prospect 
drop table if exists reporting_cast.prospect_priority;
create table reporting_cast.prospect_priority as 
select  territory_code
,salesrep_name 
,shipto_customer_number
,shipto_customer_name
,customer_shipto_zip_code
,customer_shipto_class
--2021 Requirement
,market_segment
,city
,state
,customer_shipto_site_use_id
,segments
,call_activity
,Priority
,gold_flag
,Nursing_facility_bed_cnt
,Skilled_nursing_bed_cnt 
,Assisted_Living_bed_cnt
,Rehabilitation_bed_cnt
,Hospice_bed_cnt
,(Nursing_facility_bed_cnt + Skilled_nursing_bed_cnt + Assisted_Living_bed_cnt + Rehabilitation_bed_cnt + Hospice_bed_cnt) as Tot_bed_cnt
,case when (Nursing_facility_bed_cnt + Skilled_nursing_bed_cnt + Assisted_Living_bed_cnt + Rehabilitation_bed_cnt + Hospice_bed_cnt) <=75 then 'Low'
when (Nursing_facility_bed_cnt + Skilled_nursing_bed_cnt + Assisted_Living_bed_cnt + Rehabilitation_bed_cnt + Hospice_bed_cnt) between 76 and 125 then 'Mid' 
when (Nursing_facility_bed_cnt + Skilled_nursing_bed_cnt + Assisted_Living_bed_cnt + Rehabilitation_bed_cnt + Hospice_bed_cnt) >=126 then 'High' else 'NA' end as Prospect_priority
from
(
select  
 b.territory_code
,b.salesrep_name 
,b.shipto_customer_number
,b.shipto_customer_name
,b.customer_shipto_zip_code
,b.customer_shipto_class
--2021 Requirement
,b.market_segment
,b.city
,b.state
,b.customer_shipto_site_use_id
,b.call_activity
,b.segments
,b.Priority
,b.gold_flag
,max(case when bf.bed_desc = 'Nursing Facility' then lic_bed_cnt else 0 end) as Nursing_facility_bed_cnt
,max(case when bf.bed_desc = 'Skilled Nursing' then lic_bed_cnt else 0 end) as Skilled_nursing_bed_cnt
,max(case when bf.bed_desc = 'Assisted Living' then lic_bed_cnt else 0 end) as Assisted_Living_bed_cnt
,max(case when bf.bed_desc = 'Rehabilitation' then lic_bed_cnt else 0 end) as Rehabilitation_bed_cnt
,max(case when bf.bed_desc = 'Hospice' then lic_bed_cnt else 0 end) as Hospice_bed_cnt
--,case when bf.bed_desc = 'Total' then lic_bed_cnt end as All_bed_cnt
--,lic_bed_cnt,cens_bed_cnt,stfd_bed_cnt
from reporting_cast.inside_sales_call_list_all b
left join reporting_mdm.kci_iqvia_stewardship o on b.customer_shipto_site_use_id = o.site_use
--left join sand_mdm.iqvia_customer_dim d on d.iqvia_customer_key = o.iqvia_customer_key
left join onekey.hco_bed_fact bf on bf.hco_hce_id = o.hco_hce_id
where substr(b.territory_code,1,3) = 'ISR' 
and Upper(trim(bed_desc)) in ( 'NURSING FACILITY','SKILLED NURSING', 'ASSISTED LIVING','REHABILITATION','HOSPICE') 
and b.segments ='Prospect'
group by b.territory_code
,b.salesrep_name 
,b.shipto_customer_number
,b.shipto_customer_name
,b.customer_shipto_zip_code
,b.customer_shipto_class
--2021 Requirement
,b.market_segment
,b.city
,b.state
,b.customer_shipto_site_use_id
,b.call_activity
,b.segments
,b.Priority
,b.gold_flag
) t1;
compute stats reporting_cast.prospect_priority;
--4544
select * from sand_cast.inside_sales_call_list_week_n

--Final list
drop table if exists reporting_cast.inside_sales_call_list_week_n;
create table reporting_cast.inside_sales_call_list_week_n as
select  territory_code
,replace(salesrep_name,'(TSR)','') as salesrep_name
,substr(territory_code,1,3) as jca_role
,shipto_customer_number facility_number
,shipto_customer_name facility_name
,customer_shipto_zip_code
,customer_shipto_class
--2021 Requirement
,market_segment
,city
,state
,customer_shipto_site_use_id site_use_id
,segments
,call_activity
,Priority
,gold_flag
--Update the date
,cast(now() as timestamp) as data_refresh_date
from reporting_cast.inside_sales_call_list_all 
Where call_activity ='Round call' and substr(territory_code,1,3)='TSR'

union all

select  territory_code
,replace(salesrep_name,'(TSR)','') as salesrep_name
,substr(territory_code,1,3) as jca_role
,shipto_customer_number facility_number
,shipto_customer_name facility_name
,customer_shipto_zip_code
,customer_shipto_class
--2021 Requirement
,market_segment
,city
,state
,customer_shipto_site_use_id site_use_id
,segments
,call_activity
,Priority
,gold_flag
--Update the date
,cast(now() as timestamp) as data_refresh_date
from reporting_cast.inside_sales_call_list_all where segments <>'Prospect' 
and call_activity in('Marketing call', 'Winback call' )
and substr(territory_code,1,3)='ISR'

union all

Select  c.territory_code
,replace(c.salesrep_name,'(TSR)','') as salesrep_name
,substr(c.territory_code,1,3) as jca_role
,c.shipto_customer_number facility_number
,c.shipto_customer_name facility_name
,c.customer_shipto_zip_code
,c.customer_shipto_class
--2021 Requirement
,c.market_segment
,c.city
,c.state
,c.customer_shipto_site_use_id site_use_id
,c.segments
,c.call_activity
,isnull(p.prospect_Priority,'Low') as priority
,c.gold_flag
--Update the date
,cast(now() as timestamp) as data_refresh_date
from reporting_cast.inside_sales_call_list_all c
left join reporting_cast.prospect_priority p on c.customer_shipto_site_use_id = p.customer_shipto_site_use_id
where c.segments ='Prospect'
and substr(c.territory_code,1,3)='ISR';
compute stats reporting_cast.inside_sales_call_list_week_n;
--10184 rows

--Final list

insert into reporting_cast.inside_sales_call_list --partition(data_refresh_date)
select * from reporting_cast.inside_sales_call_list_week_n;
compute stats reporting_cast.inside_sales_call_list;
